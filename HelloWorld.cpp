﻿#include <iostream>
#include <string>

int main()
{
    std::string name("qwerty123456789");

    // содержание строки
    std::cout << "string " << name << "\n";

    // длина строки
    std::cout << "length " << name.length() << "\n";

    // первый символ
    std::cout << "first symbol " << name[0] << "\n";

    // последний символ
    std::cout << "last symbol " << name[name.length() - 1] << "\n";
}